from django.urls import re_path

from favorite.views import add_or_remove

urlpatterns = [
    re_path(r'^add-or-remove$', add_or_remove, name="favorite-add-or-remove"),
]
